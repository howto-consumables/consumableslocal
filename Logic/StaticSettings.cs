namespace Logic;

public static class StaticSettings
{
    public static string DataBasePath { get; } = "./consumables_local.db";
}