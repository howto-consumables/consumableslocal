namespace Logic;

public static class StringFilter
{
    public static IEnumerable<T> SomeFieldContains<T>(this IEnumerable<T> collection, string str)
    {
        return collection.Where(item => item.CheckAllProperties<T>(str));
    }
    
    private static bool CheckAllProperties<TItem>(this TItem item, string str)
    {
        var properties = typeof(TItem).GetProperties()
            .Where(p => p.PropertyType != typeof(Guid))
            .ToList();
        
        return properties
            .Any(propertyInfo 
                => propertyInfo.GetValue(item)?
                    .ToString()?
                    .ToLower()
                    .Contains(str, StringComparison.CurrentCultureIgnoreCase)
                ?? false);
                
    }
}