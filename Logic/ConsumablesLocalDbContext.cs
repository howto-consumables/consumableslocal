using Entities;
using Microsoft.EntityFrameworkCore;

namespace Logic;

public class ConsumablesLocalDbContext : DbContext 
{
    public DbSet<Location> Locations { get; set; }
    public DbSet<DeviceType> DeviceTypes { get; set; }
    public DbSet<Consumable> Consumables { get; set; }
    public DbSet<Order> ArchivedOrders { get; set; }

    public string DbPath { get; } = StaticSettings.DataBasePath;

    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");
}


