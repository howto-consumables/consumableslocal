﻿using System.Collections.ObjectModel;
using Entities;


namespace Logic;

public class CoreLogic
{
    private IEnumerable<Order> _pendingOrders = [];
    private ConsumablesLocalDbContext _dbContext = new();
    
    #region Location

    public ReadOnlyCollection<Location> GetLocations(string str = "")
        => new (_dbContext.Locations.SomeFieldContains(str).ToList());

    public void AddLocation(Location location)
    {
        _dbContext.Locations.Add(location);
        _dbContext.SaveChanges();
    }

    public void UpdateLocation(Location updatedLocation)
    {
        // throws if not found
        var old = _dbContext.Locations.First(l => l.Id == updatedLocation.Id);
        old.Name = updatedLocation.Name;
        old.DeviceNames = updatedLocation.DeviceNames;
        old.Description = updatedLocation.Description;
        _dbContext.SaveChanges();
    }
    
    public void DeleteLocation(Location toBeDeleted)
    {
        _dbContext.Locations.Remove(toBeDeleted);
        _dbContext.SaveChanges();
    }
    #endregion

    #region DeviceType
    public ReadOnlyCollection<DeviceType> GetDeviceTypes(string str="")
        => new (_dbContext.DeviceTypes.SomeFieldContains(str).ToList());
    
    public void AddDeviceType(DeviceType deviceType)
    {
        _dbContext.DeviceTypes.Add(deviceType);
        _dbContext.SaveChanges();
    }

    public void UpdateDeviceType(DeviceType updatedDeviceType)
    {
        // throws if not found
        var old = _dbContext.DeviceTypes.First(l => l.Id == updatedDeviceType.Id);
        old.Name = updatedDeviceType.Name;
        old.Description = updatedDeviceType.Description;
        _dbContext.SaveChanges();
    }
    
    public void DeleteDeviceType(DeviceType toBeDeleted)
    {
        _dbContext.DeviceTypes.Remove(toBeDeleted);
        _dbContext.SaveChanges();
    }


    #endregion
    
    #region Consumable
    public ReadOnlyCollection<Consumable> GetConsumables(string str="")
        => new (_dbContext.Consumables.SomeFieldContains(str).ToList());
    
    public void AddConsumable(Consumable consumable)
    {
        _dbContext.Consumables.Add(consumable);
        _dbContext.SaveChanges();
    }
    
    public void UpdateConsumable(Consumable updatedConsumable)
    {
        // throws if not found
        var old = _dbContext.Consumables.First(l => l.Id == updatedConsumable.Id);
        old.Name = updatedConsumable.Name;
        old.URL = updatedConsumable.URL;
        old.DeviceName = updatedConsumable.DeviceName;
        old.Comment = updatedConsumable.Comment;
        _dbContext.SaveChanges();
    }

    public void DeleteConsumable(Consumable toBeDeleted)
    {
        _dbContext.Consumables.Remove(toBeDeleted);
        _dbContext.SaveChanges();
    }
    #endregion

    #region pending orders
    public void AddToPendingOrders(Location location, DeviceType device,
        Consumable consumable, string comment)
    {
        var order = CreateOrder(location, device, consumable, comment);
        _pendingOrders = _pendingOrders.Append(order);
    }

    public void AddToPendingOrders(Order order)
    {
        _pendingOrders = _pendingOrders.Append(order);
    }

    public void RemovePendingOrder(Order toBeRemoved)
    {
        _pendingOrders = _pendingOrders.Where( o => o != toBeRemoved);
    }

    public ReadOnlyCollection<Order> GetPendingOrders(string str="")
        => new ReadOnlyCollection<Order>(_pendingOrders.SomeFieldContains(str).ToList());
    

    private static Order CreateOrder(Location location, DeviceType device,
       Consumable consumable, string comment)
       => new ()
       {
           Id = Guid.NewGuid(),
           Location = location,
           DeviceType = device,
           Consumable = consumable,
           Comment = comment,
           DateOfCreation = DateTime.Now
       };
    #endregion

    #region archived orders
    public void ArchivePendingOrders()
    {
        foreach(Order o in _pendingOrders)
        {
            var newOrder = Order.Copy(o);
            _dbContext.ArchivedOrders.Add(newOrder);
        }
        _dbContext.SaveChanges();
        _pendingOrders = [];
    }

    public ReadOnlyCollection<Order> GetArchivedOrders(string str="")
    {
        return new ReadOnlyCollection<Order>([.. _dbContext.ArchivedOrders.SomeFieldContains(str).ToList()]);
    }
    #endregion
}
