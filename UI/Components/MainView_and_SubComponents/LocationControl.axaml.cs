using Avalonia;
using Avalonia.Input;
using Location = Entities.Location;

namespace UI.Components;

public partial class LocationControl : ClickableUserControl
{
	#region Location
	public static readonly StyledProperty<Location> LocationProperty =
		AvaloniaProperty.Register<LocationControl, Location>(nameof(Location));

	public Location Location
	{
		get => GetValue(LocationProperty);
		set => SetValue(LocationProperty, value);
	}
	#endregion


	public LocationControl()
    {
        DataContext = this;
        InitializeComponent();
    }

	public override void OnClickHandler(object o, PointerReleasedEventArgs args)
    {
		UpdateParent(Location.Name);
    }

}