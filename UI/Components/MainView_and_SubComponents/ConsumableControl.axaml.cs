using Avalonia;
using Avalonia.Input;
using Entities;

namespace UI.Components;

public partial class ConsumableControl : ClickableUserControl
{
	#region Consumable
	public static readonly StyledProperty<Consumable> ConsumableProperty =
		AvaloniaProperty.Register<ConsumableControl, Consumable>(nameof(Consumable));

	public Consumable Consumable
	{
		get => GetValue(ConsumableProperty);
		set => SetValue(ConsumableProperty, value);
	}
	#endregion


	public ConsumableControl()
	{
		DataContext = this;
		InitializeComponent();
	}

	public override void OnClickHandler(object o, PointerReleasedEventArgs args)
	{
		UpdateParent(Consumable.Name);
	}

}