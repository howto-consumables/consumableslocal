using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Logic;
using Entities;
using Location = Entities.Location;
using Avalonia.Interactivity;
using System;

namespace UI.Components;

public delegate void ActInParent(string name);

public partial class MainView : SearchableUserControl
{
    #region ... reactive props ...

    #region Instruction

    public static readonly StyledProperty<string> InstructionProperty =
        AvaloniaProperty.Register<MainView, string>(nameof(Instruction));

    public string Instruction
    {
        get => GetValue(InstructionProperty);
        set => SetValue(InstructionProperty, value);
    }

    #endregion

    #region Url

    public static readonly StyledProperty<string> UrlProperty =
        AvaloniaProperty.Register<MainView, string>(nameof(Url));

    public string Url
    {
        get => GetValue(UrlProperty);
        set => SetValue(UrlProperty, value);
    }

    #endregion

    #region LocationsPanel

    public static readonly StyledProperty<StackPanel> LocationsPanelProperty =
        AvaloniaProperty.Register<MainView, StackPanel>(nameof(LocationsPanel));

    public StackPanel LocationsPanel
    {
        get => GetValue(LocationsPanelProperty);
        set => SetValue(LocationsPanelProperty, value);
    }

    #endregion

    #region DevicesPanel

    public static readonly StyledProperty<StackPanel> DevicesPanelProperty =
        AvaloniaProperty.Register<MainView, StackPanel>(nameof(DevicesPanel));

    public StackPanel DevicesPanel
    {
        get => GetValue(DevicesPanelProperty);
        set => SetValue(DevicesPanelProperty, value);
    }

    #endregion

    #region ConsumablesPanel

    public static readonly StyledProperty<StackPanel> ConsumablesPanelProperty =
        AvaloniaProperty.Register<MainView, StackPanel>(nameof(ConsumablesPanel));

    public StackPanel ConsumablesPanel
    {
        get => GetValue(ConsumablesPanelProperty);
        set => SetValue(ConsumablesPanelProperty, value);
    }

    #endregion

    #region IsConsumableChosen

    public static readonly StyledProperty<bool> IsConsumableChosenProperty =
        AvaloniaProperty.Register<MainView, bool>(nameof(IsConsumableChosen));

    public bool IsConsumableChosen
    {
        get => GetValue(IsConsumableChosenProperty);
        set => SetValue(IsConsumableChosenProperty, value);
    }

    #endregion

    #region ConsumableSectionVisible

    public static readonly StyledProperty<bool> ConsumableSectionVisibleProperty =
        AvaloniaProperty.Register<MainView, bool>(nameof(ConsumableSectionVisible));

    public bool ConsumableSectionVisible
    {
        get => GetValue(ConsumableSectionVisibleProperty);
        set => SetValue(ConsumableSectionVisibleProperty, value);
    }

    #endregion

    #region CommentForOrder

    public static readonly StyledProperty<string> CommentForOrderProperty =
        AvaloniaProperty.Register<MainView, string>(nameof(CommentForOrder));

    public string CommentForOrder
    {
        get => GetValue(CommentForOrderProperty);
        set => SetValue(CommentForOrderProperty, value);
    }

    #endregion

    #endregion

    private Location? _chosenLocation = null;
    private DeviceType? _chosenDevice = null;
    private Consumable? _chosenConsumable = null;

    public MainView(
        Action<EventHandler<TextChangedEventArgs>> subscribeToSearchStringChangedEvent,
        Action<string> setSearchString
    ) : base(subscribeToSearchStringChangedEvent, setSearchString)
    {
        SubscribeToSearchStringChangedEvent(OnSearchStringChanged);

        LocationsPanel = new StackPanel();
        DevicesPanel = new StackPanel();
        ConsumablesPanel = new StackPanel();

        InitializeState();

        DataContext = this;
        InitializeComponent();
    }

    #region event handlers passed into children

    public void OnLocationChosen(string s)
    {
        _chosenLocation = new CoreLogic().GetLocations().First(l => l.Name == s);
        SetStateToLocationChosen(_chosenLocation);
    }

    public void OnDeviceChosen(string s)
    {
        if (_chosenLocation is null) throw new Exception("Precondition not met: _chosenLocation is null");

        _chosenDevice = _logic.GetDeviceTypes().First(d => d.Name == s);
        SetStateToDeviceChosen(_chosenLocation, _chosenDevice);
    }

    public void OnConsumableChosen(string s)
    {
        if (_chosenLocation is null) throw new Exception("Precondition not met: _chosenLocation is null");
        if (_chosenDevice is null) throw new Exception("Precondition not met: _chosenDevice is null");

        _chosenConsumable = _logic.GetConsumables().First(c => c.Name == s);
        SetStateToConsumableChosen(_chosenLocation, _chosenDevice, _chosenConsumable);
    }

    #endregion

    public void OnViewChanged()
    {
        /* do not change the state */
    }

    public void ReloadData()
    {
        SetSearchString("");
        UpdateLocations();
    }

    public void OnResetClick(object o, RoutedEventArgs args) => InitializeState();
    
    public void OnGoBackClick(object o, RoutedEventArgs args) => GoBackOneStep();

    public void OnOrderClick(object o, RoutedEventArgs args)
    {
        if (_chosenLocation is null) throw new Exception("Precondition not met: _chosenLocation is null");
        if (_chosenDevice is null) throw new Exception("Precondition not met: _chosenDevice is null");
        if (_chosenConsumable is null) throw new Exception("Precondition not met: _chosenConsumable is null");


        _logic.AddToPendingOrders(_chosenLocation, _chosenDevice, _chosenConsumable, CommentForOrder);

        GoBackOneStep();
    }
    
    private void OnSearchStringChanged(object? obj, RoutedEventArgs args)
    {
        // get newSearchString
        if (obj is not TextBox)
            throw new ArgumentException("Unexpected object passed into eventhandler.");
        var textBox = (TextBox)obj;
        var newSearchString = textBox.Text ?? "";

        // change view in function of current state
        if (AllChosen())
        {
            // do nothing
            return;
        }
        if (DeviceChosen())
        {
            UpdateConsumables(newSearchString);
            return;
        }
        if (LocationChosen())
        {
            UpdateDevices(newSearchString);
            return;
        }
        if (NothingChosen())
        {
            UpdateLocations(newSearchString);
            return;
        }
        throw new Exception("The main view has an illegal state.");
    }

    private void GoBackOneStep()
    {
        if (AllChosen())
        {
            SetStateToDeviceChosen(_chosenLocation!, _chosenDevice!);
            return;
        }
        if (DeviceChosen())
        {
            SetStateToLocationChosen(_chosenLocation!);
            return;
        }
        if (LocationChosen())
        {
            InitializeState();
        }
        if (NothingChosen())
        {
            // do nothing, this is the initial state
            return;
        }
        throw new Exception("The main view has an illegal state.");
    }
    
    # region set states

    private void InitializeState()
    {
        _chosenLocation = null;
        _chosenDevice = null;
        _chosenConsumable = null;
        CommentForOrder = string.Empty;

        UpdateLocations();

        // devices panel
        DevicesPanel.Children.Clear();

        // consumables panel
        ConsumablesPanel.Children.Clear();

        // five more
        Instruction = "Please choose a location";
        ConsumableSectionVisible = false;
        IsConsumableChosen = false;
        Url = string.Empty;
        SetSearchString("");
    }

    private void SetStateToLocationChosen(Location location)
    {
        // internal state
        _chosenLocation = location;
        _chosenDevice = null;
        _chosenConsumable = null;

        // locations panel
        LocationsPanel.Children.Clear();
        LocationsPanel.Children.Add(new TextBlock() { Text = _chosenLocation.Name });

        UpdateDevices();
        
        // consumables panel
        ConsumablesPanel.Children.Clear();

        // five more
        Instruction = "Please choose a device";
        ConsumableSectionVisible = false;
        IsConsumableChosen = false;
        Url = string.Empty;
        SetSearchString("");
    }

    private void SetStateToDeviceChosen(Location location, DeviceType device)
    {
        // internal state
        _chosenLocation = location;
        _chosenDevice = device;
        _chosenConsumable = null;

        // locations panel
        LocationsPanel.Children.Clear();
        LocationsPanel.Children.Add(new TextBlock() { Text = _chosenLocation.Name });

        // devices panel
        DevicesPanel.Children.Clear();
        DevicesPanel.Children.Add(new TextBlock() { Text = _chosenDevice.Name });

        UpdateConsumables();

        // five more
        Instruction = "Please choose a consumable";
        ConsumableSectionVisible = true;
        IsConsumableChosen = false;
        Url = string.Empty;
        SetSearchString("");
    }

    private void SetStateToConsumableChosen(Location location, DeviceType device, Consumable consumable)
    {
        // internal state
        _chosenLocation = location;
        _chosenDevice = device;
        _chosenConsumable = consumable;

        // locations panel
        LocationsPanel.Children.Clear();
        LocationsPanel.Children.Add(new TextBlock() { Text = _chosenLocation.Name });

        // devices panel
        DevicesPanel.Children.Clear();
        DevicesPanel.Children.Add(new TextBlock() { Text = _chosenDevice.Name });

        // consumables panel
        ConsumablesPanel.Children.Clear();
        ConsumablesPanel.Children.Add(new TextBlock() { Text = _chosenConsumable.Name });

        // five more
        Instruction = $"Here you can order this consumable:";
        ConsumableSectionVisible = true;
        IsConsumableChosen = true;
        Url = _chosenConsumable.URL;
        SetSearchString("");

        OrderButton.IsDefault = true;
    }

    #endregion

    #region methods that check the state
    private bool AllChosen() =>
        _chosenLocation is not null && _chosenDevice is not null && _chosenConsumable is not null;

    private bool DeviceChosen() =>
        _chosenLocation is not null && _chosenDevice is not null && _chosenConsumable is null;

    private bool LocationChosen()
        => _chosenLocation is not null && _chosenDevice is null && _chosenConsumable is null;

    private bool NothingChosen()
        => _chosenLocation is null && _chosenDevice is null && _chosenConsumable is null;
    
    #endregion
    
    #region update methods
    private void UpdateLocations(string searchString = "")
    {
        LocationsPanel.Children.Clear();
        var locations = _logic.GetLocations(searchString).ToList();
        locations.ForEach(l =>
            LocationsPanel.Children.Add(new LocationControl() { Location = l, UpdateParent = OnLocationChosen }));
    }

    private void UpdateConsumables(string searchString = "")
    {
        if(_chosenDevice is null) throw new Exception("Precondition not met: _chosenDevice is null");
        
        ConsumablesPanel.Children.Clear();
        var consumables = _logic.GetConsumables(searchString)
            .Where(c => c.DeviceName == _chosenDevice.Name)
            .ToList();
        consumables.ForEach(c => ConsumablesPanel.Children.Add(new ConsumableControl()
            { Consumable = c, UpdateParent = OnConsumableChosen }));
    }

    private void UpdateDevices(string searchString = "")
    {
        if(_chosenLocation is null) throw new Exception("Precondition not met: _chosenLocation is null");
        
        DevicesPanel.Children.Clear();
        var devices = _logic.GetDeviceTypes(searchString)
            .Where(d => _chosenLocation.DeviceNames.Contains(d.Name))
            .ToList();
        devices.ForEach(d =>
            DevicesPanel.Children.Add(new DeviceControl() { Device = d, UpdateParent = OnDeviceChosen }));
    }
    #endregion
}