using Avalonia;
using Avalonia.Input;
using Entities;

namespace UI.Components;

public partial class DeviceControl : ClickableUserControl
{
	#region Device
	public static readonly StyledProperty<DeviceType> DeviceProperty =
		AvaloniaProperty.Register<DeviceControl, DeviceType>(nameof(Device));

	public DeviceType Device
	{
		get => GetValue(DeviceProperty);
		set => SetValue(DeviceProperty, value);
	}
	#endregion


	public DeviceControl()
	{
		DataContext = this;
		InitializeComponent();
	}

	public override void OnClickHandler(object o, PointerReleasedEventArgs args)
	{
		UpdateParent(Device.Name);
	}

}