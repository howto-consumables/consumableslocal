using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Entities;
using Logic;


namespace UI.Components;

public partial class ArchivedOrdersView : SearchableUserControl
{
    #region ArchivedOrders

    public static readonly StyledProperty<IEnumerable<Order>> ArchivedOrdersProperty =
        AvaloniaProperty.Register<MainView, IEnumerable<Order>>(nameof(ArchivedOrders));

    public IEnumerable<Order> ArchivedOrders
    {
        get => GetValue(ArchivedOrdersProperty);
        set => SetValue(ArchivedOrdersProperty, value);
    }

    #endregion

    #region StartDate

    public static readonly StyledProperty<DateTime> StartDateProperty =
        AvaloniaProperty.Register<MainView, DateTime>(nameof(StartDate));

    public DateTime StartDate
    {
        get => GetValue(StartDateProperty);
        set => SetValue(StartDateProperty, value);
    }

    #endregion

    #region EndDate

    public static readonly StyledProperty<DateTime> EndDateProperty =
        AvaloniaProperty.Register<MainView, DateTime>(nameof(EndDate));

    public DateTime EndDate
    {
        get => GetValue(EndDateProperty);
        set => SetValue(EndDateProperty, value);
    }

    #endregion

    public ArchivedOrdersView(
        Action<EventHandler<TextChangedEventArgs>> subscribeToSearchStringChangedEvent,
        Action<string> setSearchString
    ) : base(subscribeToSearchStringChangedEvent, setSearchString)
    {
        SubscribeToSearchStringChangedEvent(OnSearchStringChanged);
        
        Initialize();
        DataContext = this;
        InitializeComponent();
    }

    public void LoadAndFilterArchivedOrders(string searchString = "")
    {
        ArchivedOrders = _logic.GetArchivedOrders(searchString)
            .OrderByDescending(a => a.DateOfCreation)
            .Where(a => a.DateOfCreation >= StartDate && a.DateOfCreation <= EndDate);
    }

    public void OnResetClick(object obj, RoutedEventArgs args)
    {
        ResetDates();
    }

    public void OnDateSelected(object obj, SelectionChangedEventArgs args)
    {
        // think about having the searchString injected
        // or accessing it directly in visual parent
        // that way, we could keep the search after setting some dates
        SetSearchString("");
        LoadAndFilterArchivedOrders();
    }

    private void Initialize()
    {
        SetSearchString("");
        ResetDates();
        LoadAndFilterArchivedOrders();
    }

    private void ResetDates()
    {
        StartDate = new DateTime(2020, 1, 1);

        // set to the end of the day
        EndDate = DateTime.Today.AddDays(1).AddMicroseconds(-1);
    }

    private void OnSearchStringChanged(object? obj, RoutedEventArgs args)
    {
        if (obj is not TextBox)
            throw new ArgumentException("Unexpected object passed into eventhandler.");
        var textBox = (TextBox)obj;
        var newSearchString = textBox.Text ?? "";
        LoadAndFilterArchivedOrders(newSearchString);
    }
}