


using Avalonia.Controls;
using Avalonia.Input;

namespace UI.Components;


public abstract class ClickableUserControl : UserControl
{
	public abstract void OnClickHandler(object sender, PointerReleasedEventArgs args);
    public required ActInParent UpdateParent { get; set; }

}