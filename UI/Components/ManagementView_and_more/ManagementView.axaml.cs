using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Entities;
using UI.Windows;
using Location = Entities.Location;

namespace UI.Components;

public partial class ManagementView : SearchableUserControl
{
    public List<string> ToBeManagedChoice { get; } = ["", "Location", "DeviceType", "Consumable"];
    private readonly Func<string> GetSearchString;
    
    #region Reatcive Props
    
    #region ToBeManaged
    public static readonly StyledProperty<string?> ToBeManagedProperty =
        AvaloniaProperty.Register<ManagementView, string?>(nameof(ToBeManaged));
 
    public string? ToBeManaged
    {
        get => GetValue(ToBeManagedProperty);
        set => SetValue(ToBeManagedProperty, value);
    }
    #endregion
    
    #region LocationList
    public static readonly StyledProperty<List<Location>> LocationListProperty =
        AvaloniaProperty.Register<ManagementView, List<Location>>(nameof(LocationList));
 
    public List<Location> LocationList
    {
        get => GetValue(LocationListProperty);
        set => SetValue(LocationListProperty, value);
    }
    #endregion
    
    #region DeviceTypeList
    public static readonly StyledProperty<List<DeviceType>> DeviceTypeListProperty =
        AvaloniaProperty.Register<ManagementView, List<DeviceType>>(nameof(DeviceTypeList));
 
    public List<DeviceType> DeviceTypeList
    {
        get => GetValue(DeviceTypeListProperty);
        set => SetValue(DeviceTypeListProperty, value);
    }
    #endregion

    #region ConsumableList
    public static readonly StyledProperty<List<Consumable>> ConsumableListProperty =
        AvaloniaProperty.Register<ManagementView, List<Consumable>>(nameof(ConsumableList));
 
    public List<Consumable> ConsumableList
    {
        get => GetValue(ConsumableListProperty);
        set => SetValue(ConsumableListProperty, value);
    }
    #endregion
    
    #endregion
    
    public ManagementView(
        Action<EventHandler<TextChangedEventArgs>> subscribeToSearchStringChangedEvent,
        Action<string> setSearchString,
        Func<string> getSearchString
        ) : base(subscribeToSearchStringChangedEvent, setSearchString)
    {
        SubscribeToSearchStringChangedEvent(OnSearchStringChanged);
        GetSearchString = getSearchString;
        
        DataContext = this;
        Initialize();
    }

    private void UpdateManagingState()
    {
        if (ToBeManaged == ToBeManagedChoice[0] || ToBeManaged == null) SetStateToInitialized();
        else if (ToBeManaged == ToBeManagedChoice[1]) SetStateToManagingLocation();
        else if (ToBeManaged == ToBeManagedChoice[2]) SetStateToManagingDeviceType();
        else if (ToBeManaged == ToBeManagedChoice[3]) SetStateToManagingConsumable();
    }

    #region state setters
    
    // public so it can be called from other windows
    public void Initialize()
    {
        // after closing other windows initializeComponent must be called
        // in order not to have a loop in the ComboBox SelectionChanged event
        InitializeComponent();
        SetStateToInitialized();
    }

    private void SetStateToInitialized()
    {
        ToBeManaged = null;
        LocationList = _logic.GetLocations(GetSearchString()).ToList();
        DeviceTypeList = _logic.GetDeviceTypes(GetSearchString()).ToList();
        ConsumableList = _logic.GetConsumables(GetSearchString()).ToList();
        AddLocationButton.IsVisible = true;
        AddDeviceTypeButton.IsVisible = true;
        AddConsumableButton.IsVisible = true;
    }
    
    private void SetStateToManagingLocation()
    {
        LocationList = _logic.GetLocations(GetSearchString()).ToList();
        DeviceTypeList = [];
        ConsumableList = [];
        AddLocationButton.IsVisible = true;
        AddDeviceTypeButton.IsVisible = false;
        AddConsumableButton.IsVisible = false;
        
    }
    
    private void SetStateToManagingDeviceType()
    {
        LocationList = [];
        DeviceTypeList = _logic.GetDeviceTypes(GetSearchString()).ToList();
        ConsumableList = [];
        AddLocationButton.IsVisible = false;
        AddDeviceTypeButton.IsVisible = true;
        AddConsumableButton.IsVisible = false;
    }
   
    private void SetStateToManagingConsumable()
    {
        LocationList = [];
        DeviceTypeList = [];
        ConsumableList = _logic.GetConsumables(GetSearchString()).ToList();
        AddLocationButton.IsVisible = false;
        AddDeviceTypeButton.IsVisible = false;
        AddConsumableButton.IsVisible = true;
    }
    
    #endregion

    #region event handlers
    private void OnComboBoxSelectionChanged(object sender, SelectionChangedEventArgs _)
    {
        if (sender is ComboBox comboBox)
        {
            if (comboBox.SelectedItem is string selectedItem)
            {
                ToBeManaged = selectedItem;
                UpdateManagingState();
            }
        }
    }
    

    private void AddLocationClick(object sender, RoutedEventArgs _)
        => UpdateEntityWindow.CreateLocationAddWindow(GetCurrentWindow()).Show();
    
    private void AddDeviceTypeClick(object sender, RoutedEventArgs _)
        => UpdateEntityWindow.CreateDeviceTypeAddWindow(GetCurrentWindow()).Show();
    
    private void AddConsumableClick(object sender, RoutedEventArgs _)
        => UpdateEntityWindow.CreateConsumableAddWindow(GetCurrentWindow()).Show();

    private void OnLocationSelectedChanged(object? sender, SelectionChangedEventArgs _)
    {
        if (sender is ListBox listBox)
        {
            var selectedItem = listBox.SelectedItem;
            if (selectedItem is Location location)
            {
                UpdateEntityWindow.CreateLocationUpdateWindow(GetCurrentWindow(), location).Show();
            }
        }
    }
    
    private void OnDeviceTypeSelectedChanged(object? sender, SelectionChangedEventArgs _)
    {
        if (sender is ListBox listBox)
        {
            var selectedItem = listBox.SelectedItem;
            if (selectedItem is DeviceType deviceType)
            {
                UpdateEntityWindow.CreateDeviceTypeUpdateWindow(GetCurrentWindow(), deviceType).Show();
            }
        }
    }
    
    private void OnConsumableSelectedChanged(object? sender, SelectionChangedEventArgs _)
    {
        if (sender is ListBox listBox)
        {
            var selectedItem = listBox.SelectedItem;
            if (selectedItem is Consumable consumable)
            {
                UpdateEntityWindow.CreateConsumableUpdateWindow(GetCurrentWindow(), consumable).Show();
            }
        }
    }
    
    private void OnSearchStringChanged(object? obj, RoutedEventArgs args)
    {
        UpdateManagingState();
    }
    #endregion
    
    private MainWindow GetCurrentWindow()
    {
        return this.VisualRoot as MainWindow ?? throw new Exception("Visual root is not a window");
    }
}