using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Logic;
using UI.Windows;
using Location = Entities.Location;
namespace UI.Components.UpdateEntityControls;

public partial class UpdateLocationControl : AllowsUpdateControl
{
    private readonly Location _location;
    
    #region LocationName
    public static readonly StyledProperty<string> LocationNameProperty =
        AvaloniaProperty.Register<UpdateLocationControl, string>(nameof(LocationName));
 
    public string LocationName
    {
        get => GetValue(LocationNameProperty);
        set => SetValue(LocationNameProperty, value);
    }
    #endregion
    
    #region LocationDeviceNames
    public static readonly StyledProperty<List<string>> LocationDeviceNamesProperty =
        AvaloniaProperty.Register<UpdateLocationControl, List<string>>(nameof(LocationDeviceNames));
 
    public List<string> LocationDeviceNames
    {
        get => GetValue(LocationDeviceNamesProperty);
        set => SetValue(LocationDeviceNamesProperty, value);
    }
    #endregion
    
    #region LocationDescription
    public static readonly StyledProperty<string> LocationDescriptionProperty =
        AvaloniaProperty.Register<UpdateLocationControl, string>(nameof(LocationDescription));
 
    public string LocationDescription
    {
        get => GetValue(LocationDescriptionProperty);
        set => SetValue(LocationDescriptionProperty, value);
    }
    #endregion
    
    public UpdateLocationControl(Location location)
    {
        DataContext = this;
        InitializeComponent();
        _location = location;
        LocationName = _location.Name ?? "";
        LocationDeviceNames = _location.DeviceNames ?? [];
        LocationDescription = _location.Description ?? "";
        InfoText = "";
    }

    public override void AddButtonClick()
    {
        if (!InputIsValid()) return;

        UpdateLocationField();
        _logic.AddLocation(_location);
        
        CloseCurrentWindow();
    }
    
    public override void UpdateButtonClick()
    {
        if (!InputIsValid()) return;

        UpdateLocationField();
        _logic.UpdateLocation(_location);
        
        CloseCurrentWindow();
    }
    
    private bool InputIsValid()
    {
        if (LocationName == "")
        {
            InfoText = "Please set a name";
            return false;
        }
        if (LocationDeviceNames?.Count == 0)
        {
            InfoText = "Please set at least one device type";
            return false;
        }
        return true;
    }

    private void UpdateLocationField()
    {
        _location.Name = LocationName;
        _location.DeviceNames = LocationDeviceNames;
        _location.Description = LocationDescription;
    }
    
    public override void DeleteButtonClick()
    {
        _logic.DeleteLocation(_location);
        CloseCurrentWindow();
    }
    
    private void ModifyDeviceNamesClick(object? sender, RoutedEventArgs e)
    {
        var current = LocationDeviceNames ?? [];
        var updateDeviceNamesWindow = new UpdateDeviceNamesWindow(
            current,
            (List<string> newList) => LocationDeviceNames = newList);
        var owningCurrentWindow = GetCurrentWindow();
        updateDeviceNamesWindow.ShowDialog(owningCurrentWindow);
    }
}