using System;
using Avalonia;
using Avalonia.Controls;
using Logic;
using UI.Windows;

namespace UI.Components.UpdateEntityControls;

public abstract class AllowsUpdateControl : UserControl
{
    #region InfoText
    public static readonly StyledProperty<string> InfoTextProperty =
        AvaloniaProperty.Register<UpdateLocationControl, string>(nameof(InfoText));
 
    public string InfoText
    {
        get => GetValue(InfoTextProperty);
        set => SetValue(InfoTextProperty, value);
    }
    #endregion
    
    protected readonly CoreLogic _logic = App.Services.GetService(typeof(CoreLogic)) as CoreLogic
                                        ?? throw new Exception("Dependency injection failed.");

    public abstract void AddButtonClick();
    public abstract void UpdateButtonClick();
    public abstract void DeleteButtonClick();
    
    protected Window GetCurrentWindow()
        => this.VisualRoot as Window ?? throw new Exception("Visual root is not a window");

    protected void CloseCurrentWindow()
        => GetCurrentWindow().Close();
}