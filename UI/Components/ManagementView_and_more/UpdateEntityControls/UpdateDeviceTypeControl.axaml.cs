using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Entities;

namespace UI.Components.UpdateEntityControls;

public partial class UpdateDeviceTypeControl : AllowsUpdateControl
{
    private readonly DeviceType _deviceType;
    
    #region DeviceTypeName
    public static readonly StyledProperty<string> DeviceTypeNameProperty =
        AvaloniaProperty.Register<UpdateDeviceTypeControl, string>(nameof(DeviceTypeName));
 
    public string DeviceTypeName
    {
        get => GetValue(DeviceTypeNameProperty);
        set => SetValue(DeviceTypeNameProperty, value);
    }
    #endregion
    
    #region DeviceTypeDescription
    public static readonly StyledProperty<string> DeviceTypeDescriptionProperty =
        AvaloniaProperty.Register<UpdateDeviceTypeControl, string>(nameof(DeviceTypeDescription));
 
    public string DeviceTypeDescription
    {
        get => GetValue(DeviceTypeDescriptionProperty);
        set => SetValue(DeviceTypeDescriptionProperty, value);
    }
    #endregion
    
    public UpdateDeviceTypeControl(DeviceType deviceType)
    {
        DataContext = this;
        InitializeComponent();
        _deviceType = deviceType;
        DeviceTypeName = _deviceType.Name ?? "";
        DeviceTypeDescription = _deviceType.Description ?? "";
        InfoText = "";
    }
    
    public override void AddButtonClick()
    {
        if (!InputIsValid()) return;
        
        UpdateDeviceTypeField();
        _logic.AddDeviceType(_deviceType);

        CloseCurrentWindow();
    }
    
    public override void UpdateButtonClick()
    {
        if (!InputIsValid()) return;
        
        UpdateDeviceTypeField();
        _logic.UpdateDeviceType(_deviceType);
        
        CloseCurrentWindow();
    }

    private bool InputIsValid()
    {
        if (DeviceTypeName == "")
        {
            InfoText = "Please set a name";
            return false;
        }
        return true;
    }

    private void UpdateDeviceTypeField()
    {
        _deviceType.Name = DeviceTypeName;
        _deviceType.Description = DeviceTypeDescription;
    }

    public override void DeleteButtonClick()
    {
        _logic.DeleteDeviceType(_deviceType);
        CloseCurrentWindow();
    }
}