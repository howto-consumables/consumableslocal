using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Entities;

namespace UI.Components.UpdateEntityControls;

public partial class UpdateConsumableControl : AllowsUpdateControl
{
    private readonly Consumable _consumable;

    public List<string> DeviceNameOptions { get; init;  } 
    
    #region ReactiveProps
    
    #region ConsumableName
    public static readonly StyledProperty<string> ConsumableNameProperty =
        AvaloniaProperty.Register<UpdateConsumableControl, string>(nameof(ConsumableName));
 
    public string ConsumableName
    {
        get => GetValue(ConsumableNameProperty);
        set => SetValue(ConsumableNameProperty, value);
    }
    #endregion
    
    #region ConsumableDeviceName
    public static readonly StyledProperty<string> ConsumableDeviceNameProperty =
        AvaloniaProperty.Register<UpdateConsumableControl, string>(nameof(ConsumableDeviceName));
 
    public string ConsumableDeviceName
    {
        get => GetValue(ConsumableDeviceNameProperty);
        set => SetValue(ConsumableDeviceNameProperty, value);
    }
    #endregion
    
    #region ConsumableUrl
    public static readonly StyledProperty<string> ConsumableUrlProperty =
        AvaloniaProperty.Register<UpdateConsumableControl, string>(nameof(ConsumableUrl));
 
    public string ConsumableUrl
    {
        get => GetValue(ConsumableUrlProperty);
        set => SetValue(ConsumableUrlProperty, value);
    }
    #endregion
    
    #region ConsumableComment
    public static readonly StyledProperty<string> ConsumableCommentProperty =
        AvaloniaProperty.Register<UpdateConsumableControl, string>(nameof(ConsumableComment));
 
    public string ConsumableComment
    {
        get => GetValue(ConsumableCommentProperty);
        set => SetValue(ConsumableCommentProperty, value);
    }
    #endregion
    
    #endregion
    
    
    public UpdateConsumableControl(Consumable consumable)
    {
        _consumable = consumable;
        
        ConsumableName = _consumable.Name ?? "";
        ConsumableUrl = _consumable.URL ?? "";
        DeviceNameOptions = _logic.GetDeviceTypes()
            .Select(item => item.Name).Prepend("").ToList();
        ConsumableDeviceName = _consumable.DeviceName ?? "";
        ConsumableComment = _consumable.Comment ?? "";
        InfoText = "";
        
        DataContext = this;
        InitializeComponent();
    }
    
    public override void AddButtonClick()
    {
        if (!InputIsValid()) return;
        
        UpdateConsumableField();
        _logic.AddConsumable(_consumable);

        CloseCurrentWindow();
    }
    
    public override void UpdateButtonClick()
    {
        if (!InputIsValid()) return;
        
        UpdateConsumableField();
        _logic.UpdateConsumable(_consumable);
        
        CloseCurrentWindow();
    }

    private bool InputIsValid()
    {
        if (ConsumableName == "")
        {
            InfoText = "Please set a name";
            return false;
        }
        if (ConsumableUrl == "")
        {
            InfoText = "Please set an url";
            return false;
        }
        if (ConsumableDeviceName == "")
        {
            InfoText = "Please set a device name";
            return false;
        }
        return true;
    }

    private void UpdateConsumableField()
    {
        _consumable.Name = ConsumableName;
        _consumable.URL = ConsumableUrl;
        _consumable.DeviceName = ConsumableDeviceName;
        _consumable.Comment = ConsumableComment;
    }

    public override void DeleteButtonClick()
    {
        _logic.DeleteConsumable(_consumable);
        CloseCurrentWindow();
    }
}