using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Entities;
using Logic;


namespace UI.Components;

public partial class PendingOrdersView : SearchableUserControl
{
    #region PendingOrders

    public static readonly StyledProperty<IEnumerable<Order>> PendingOrdersProperty =
        AvaloniaProperty.Register<MainView, IEnumerable<Order>>(nameof(PendingOrders));

    public IEnumerable<Order> PendingOrders
    {
        get => GetValue(PendingOrdersProperty);
        set => SetValue(PendingOrdersProperty, value);
    }

    #endregion

    #region Selected

    public static readonly StyledProperty<Order> SelectedProperty =
        AvaloniaProperty.Register<MainView, Order>(nameof(Selected));

    public Order Selected
    {
        get => GetValue(SelectedProperty);
        set => SetValue(SelectedProperty, value);
    }

    #endregion

    public PendingOrdersView(
        Action<EventHandler<TextChangedEventArgs>> subscribeToSearchStringChangedEvent,
        Action<string> setSearchString
    ) : base(subscribeToSearchStringChangedEvent, setSearchString)
    {
        SubscribeToSearchStringChangedEvent(OnSearchStringChanged);
        DataContext = this;
        InitializeComponent();
    }

    public void OnArchiveClicked(object sender, RoutedEventArgs args)
    {
        ArchivePendingOrders();
        LoadPendingOrders();
    }

    public void OnSelectionChanged(object o, SelectionChangedEventArgs args)
    {
        var item = ((ListBox?)args?.Source)?.SelectedItem;
        var id = ((Order?)item)?.Id;

        // event also triggered when the selected item is removed
        if (id is not null) OnOrderSelected((Guid)id);
    }

    public void OnOrderSelected(Guid id)
    {
        var all = _logic.GetPendingOrders();
        var found = all.First(e => e.Id == id);
        Selected = found;
    }

    public void OnDeleteSelected(object sender, RoutedEventArgs args)
    {
        _logic.RemovePendingOrder(Selected);
        LoadPendingOrders();
    }

    public void LoadPendingOrders(string filterString = "")
    {
        PendingOrders = _logic.GetPendingOrders(filterString);
    }

    private void ArchivePendingOrders()
    {
        _logic.ArchivePendingOrders();
    }

    private void OnSearchStringChanged(object? obj, RoutedEventArgs args)
    {
        if (obj is not TextBox)
            throw new ArgumentException("Unexpected object passed into eventhandler.");
        var textBox = (TextBox)obj;
        var newSearchString = textBox.Text ?? "";
        LoadPendingOrders(newSearchString);
    }
    
}