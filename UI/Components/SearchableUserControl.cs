using System;
using Avalonia.Controls;
using Logic;

namespace UI.Components;

public class SearchableUserControl(
    Action<EventHandler<TextChangedEventArgs>> subscribeToSearchStringChangedEvent,
    Action<string> setSearchString)
    : UserControl
{
    protected readonly CoreLogic _logic = App.Services.GetService(typeof(CoreLogic)) as CoreLogic
                                          ?? throw new Exception("Dependency injection failed.");
    protected Action<EventHandler<TextChangedEventArgs>> SubscribeToSearchStringChangedEvent { get; } = subscribeToSearchStringChangedEvent;
    protected Action<string> SetSearchString { get; } = setSearchString;
    
}