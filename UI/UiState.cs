





using Avalonia.Controls;
using Entities;
using Location = Entities.Location;

namespace UI;


class GlobalState
{
    public UserControl? CurrentView { get; set; }
    public Location? ChosenLocation { get; set; }
    public DeviceType? ChosenDevice { get; set; }
    public Consumable? ChosenConsumable { get; set; }
}