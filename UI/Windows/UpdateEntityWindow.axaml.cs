using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Entities;
using Location = Entities.Location;
using UI.Components.UpdateEntityControls;
using UI.Options;

namespace UI.Windows;

public partial class UpdateEntityWindow : Window
{
    private MainWindow MainWindow;
    public UpdateEntityWindowButtonOptions ButtonOptions { get; }
    
    #region configuration
    public string Instruction { get; }
    public bool DeleteVisible { get; }
    
    #endregion
    

    #region reactive properties
    
    #region Content
    public static readonly StyledProperty<AllowsUpdateControl> ContentProperty =
        AvaloniaProperty.Register<UpdateEntityWindow, AllowsUpdateControl>(nameof(Content));
    public AllowsUpdateControl Content
    {
        get => GetValue(ContentProperty);
        set => SetValue(ContentProperty, value);
    }
    #endregion
    
    #region IsUpdateButtonEnabled
    public static readonly StyledProperty<bool> IsUpdateButtonEnabledProperty =
        AvaloniaProperty.Register<UpdateEntityWindow, bool>(nameof(IsUpdateButtonEnabled));
    public bool IsUpdateButtonEnabled
    {
        get => GetValue(IsUpdateButtonEnabledProperty);
        set => SetValue(IsUpdateButtonEnabledProperty, value);
    }
    #endregion

    #endregion

    public static UpdateEntityWindow CreateLocationAddWindow(MainWindow mainWindow)
    {
        var newLocation = new Location();
        var updateLocationControl = new UpdateLocationControl(newLocation);
        
        return new UpdateEntityWindow(
            mainWindow,
            "Add Location",
            "",
            true,
            updateLocationControl,
            UpdateEntityWindowButtonOptions.GetAddOptions()
            );
    }

    public static UpdateEntityWindow CreateLocationUpdateWindow(MainWindow mainWindow, Location location)
    {
        var updateLocationControl = new UpdateLocationControl(location);
        return new UpdateEntityWindow(
            mainWindow,
            "Update Location",
            "",
            true,
            updateLocationControl,
            UpdateEntityWindowButtonOptions.GetUpdateOptions()
        );
    }
    
    public static UpdateEntityWindow CreateDeviceTypeAddWindow(MainWindow mainWindow)
    {
        var newDeviceType = new DeviceType();
        var updateDeviceTypeControl = new UpdateDeviceTypeControl(newDeviceType);
        
        return new UpdateEntityWindow(
            mainWindow,
            "Add DeviceType",
            "",
            true,
            updateDeviceTypeControl,
            UpdateEntityWindowButtonOptions.GetAddOptions()
        );
    }
    
    public static UpdateEntityWindow CreateDeviceTypeUpdateWindow(MainWindow mainWindow, DeviceType deviceType)
    {
        var updateDeviceTypeControl = new UpdateDeviceTypeControl(deviceType);
        return new UpdateEntityWindow(
            mainWindow,
            "Update DeviceType",
            "",
            true,
            updateDeviceTypeControl,
            UpdateEntityWindowButtonOptions.GetUpdateOptions()
        );
    }
    
    public static UpdateEntityWindow CreateConsumableAddWindow(MainWindow mainWindow)
    {
        var newConsumable = new Consumable();
        var updateConsumableControl = new UpdateConsumableControl(newConsumable);
        
        return new UpdateEntityWindow(
            mainWindow,
            "Add Consumable",
            "",
            true,
            updateConsumableControl,
            UpdateEntityWindowButtonOptions.GetAddOptions()
        );
    }
    
    public static UpdateEntityWindow CreateConsumableUpdateWindow(MainWindow mainWindow, Consumable consumable)
    {
        var updateConsumableControl = new UpdateConsumableControl(consumable);
        return new UpdateEntityWindow(
            mainWindow,
            "Update Consumable",
            "",
            true,
            updateConsumableControl,
            UpdateEntityWindowButtonOptions.GetUpdateOptions()
        );
    }
    
    
    
    private UpdateEntityWindow(
        MainWindow mainWindow,
        string customTitle,
        string instruction,
        bool deleteVisible,
        AllowsUpdateControl content,
        UpdateEntityWindowButtonOptions buttonOptions
        )
    {
        MainWindow = mainWindow;
        Title = customTitle;
        Instruction = instruction;
        DeleteVisible = deleteVisible;
        Content = content;
        ButtonOptions = buttonOptions;
        
        DataContext = this;
        InitializeComponent();
    }
    
    private void AddButtonClick(object? sender, RoutedEventArgs e)
        => Content.AddButtonClick();

    private void UpdateButtonClick(object? sender, RoutedEventArgs e)
        => Content.UpdateButtonClick();

    private void DeleteButtonClick(object? sender, RoutedEventArgs e)
        => Content.DeleteButtonClick();

    private void OnWindowClosing(object? sender, WindowClosingEventArgs e)
    {
        MainWindow.ReloadMainAndMgmtData();
    }
}