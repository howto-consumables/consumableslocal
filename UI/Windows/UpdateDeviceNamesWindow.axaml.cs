using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Logic;

namespace UI.Windows;

public partial class UpdateDeviceNamesWindow : Window
{
    protected readonly CoreLogic _logic = App.Services.GetService(typeof(CoreLogic)) as CoreLogic
                                          ?? throw new Exception("Dependency injection failed.");
    public List<string> AllDeviceTypeNames { get; set; }
    private Action<List<string>> _setListForLocation;
    
    #region CurrentlySelectedItems
    public static readonly StyledProperty<List<string>> CurrentlySelectedItemsProperty =
        AvaloniaProperty.Register<UpdateDeviceNamesWindow, List<string>>(nameof(CurrentlySelectedItems));
 
    public List<string> CurrentlySelectedItems
    {
        get => GetValue(CurrentlySelectedItemsProperty);
        set => SetValue(CurrentlySelectedItemsProperty, value);
    }
    #endregion

    public UpdateDeviceNamesWindow(List<string> currentList, Action<List<string>> setListForLocation)
    {
        _setListForLocation = setListForLocation;
        
        AllDeviceTypeNames = _logic.GetDeviceTypes().Select(item => item.Name).ToList();
        CurrentlySelectedItems = currentList;
        DataContext = this;
        InitializeComponent();
    }

    private void Update(object? sender, RoutedEventArgs e)
    {
        var newList = DeviceTypesListBox.SelectedItems?.Cast<string>().ToList();
        if (newList is null || newList.Count == 0)
        {
            // show message: please delete location entirely
        }
        else
        {
            _setListForLocation(newList);
            this.Close();
        }
    }
}