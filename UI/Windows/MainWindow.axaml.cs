using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Logic;
using UI.Components;

namespace UI.Windows;

public partial class MainWindow : Window
{
	private MainView _mainView { get; init; }
	private PendingOrdersView _pendingOrdersView { get; }
	private ArchivedOrdersView _archivedOrdersView { get; }
	private ManagementView _managementView { get; }

	#region ActiveView
	public static readonly StyledProperty<UserControl> ActiveViewProperty =
	AvaloniaProperty.Register<MainWindow, UserControl>(nameof(ActiveView));
	public UserControl ActiveView
	{
		get => GetValue(ActiveViewProperty);
		set => SetValue(ActiveViewProperty, value);
	}
	#endregion
	
	#region SearchString
	public static readonly StyledProperty<string> SearchStringProperty =
		AvaloniaProperty.Register<MainView, string>(nameof(SearchString));

	public string SearchString
	{
		get => GetValue(SearchStringProperty);
		set => SetValue(SearchStringProperty, value);
	}
	#endregion

    public MainWindow()
    {
		var logic = new CoreLogic();
        DataContext = this;
        InitializeComponent();
        
        _mainView = new(
	        SubscribeHandlerToSearchStringChangedEvent,
	        SetSearchStringInMainWindow);
        _pendingOrdersView = new(
	        SubscribeHandlerToSearchStringChangedEvent,
	        SetSearchStringInMainWindow);
        _archivedOrdersView = new(
	        SubscribeHandlerToSearchStringChangedEvent,
	        SetSearchStringInMainWindow);
        _managementView = new(
	        SubscribeHandlerToSearchStringChangedEvent,
	        SetSearchStringInMainWindow,
	        GetSearchStringInMainWindow);

		ActiveView = _mainView;
    }
    
	#region eventlisteners for nav buttons
	public void OnMainClicked(object sender, RoutedEventArgs args)
    {
        _mainView.OnViewChanged();
        ActiveView = _mainView;
    }

	public void ReloadMainAndMgmtData()
	{
		_mainView.ReloadData();
		_managementView.Initialize();
	}

	public void OnPendingClicked(object sender, RoutedEventArgs args)
	{
        _pendingOrdersView.LoadPendingOrders();
        ActiveView = _pendingOrdersView;
	}

	public void OnArchivedClicked(object sender, RoutedEventArgs args)
	{
        _archivedOrdersView.LoadAndFilterArchivedOrders();
        ActiveView = _archivedOrdersView;
	}
	public void OnMgmtClicked(object sender, RoutedEventArgs args)
	{
		//_managementView.LoadAndFilterArchivedOrders();
        ActiveView = _managementView;
	}
	#endregion
	
	private void SubscribeHandlerToSearchStringChangedEvent(EventHandler<TextChangedEventArgs> handler)
		=> SearchStringTextBox.TextChanged += handler;
	
	private void SetSearchStringInMainWindow(string str) => SearchString = str;

	private string GetSearchStringInMainWindow() => SearchString;
}