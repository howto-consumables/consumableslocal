namespace UI.Options;

public class UpdateEntityWindowButtonOptions
{
    public bool ShowAdd { get; init; }
    public bool ShowUpdate { get; init; }
    public bool ShowDelete { get; init; }

    public static UpdateEntityWindowButtonOptions GetAddOptions() => new()
    {
        ShowAdd = true,
        ShowUpdate = false,
        ShowDelete = false
    };
    
    public static UpdateEntityWindowButtonOptions GetUpdateOptions() => new()
    {
        ShowAdd = false,
        ShowUpdate = true,
        ShowDelete = true
    };
}