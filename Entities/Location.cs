﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Entities;

#nullable disable
[Owned]
public class Location
{
    [Key]
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<string> DeviceNames { get; set; }
    public string Description { get; set; }

    public class LocationComparer : IEqualityComparer<Location>
    {
        public bool Equals(Location x, Location y)
        {
            if (x is null || y is null) throw new NullReferenceException();

            return x.Name == y.Name && 
                Enumerable.SequenceEqual(x.DeviceNames, y.DeviceNames) &&
                x.Description == y.Description;
        }

        public int GetHashCode([DisallowNull] Location obj)
        {
            return obj.Name.Concat(obj.DeviceNames.GetHashCode().ToString()).GetHashCode();
        }
    }

    public static Location Copy(Location l)
    {
        List<string> devices = [];
        l.DeviceNames.ForEach( n => devices.Add(n) );

        return new()
        {
            Name = l.Name,
            DeviceNames = devices,
            Description = l.Description
        };
    }

    public override bool Equals(object obj)
    {
        if (obj is not Location) return base.Equals(obj);

        Location other = (Location)obj;
        return this.Name == other.Name && 
                Enumerable.SequenceEqual(this.DeviceNames, other.DeviceNames) &&
                this.Description == other.Description;
    }
    
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    
    public override string ToString()
    {
        
        return $"""
                    Name = {Name},
                    DeviceNames = {string.Join(", ", DeviceNames)},
                    Description = {Description}
                """;
    }
}
