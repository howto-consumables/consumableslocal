using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Entities;

#nullable disable
[Owned]
public class DeviceType
{
    [Key]
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }

    public class DeviceTypeComparer : IEqualityComparer<DeviceType>
    {
        public bool Equals(DeviceType x, DeviceType y)
        {
            if (x is null || y is null) throw new NullReferenceException();

            return x.Name == y.Name &&
                x.Description == y.Description;
        }

        public int GetHashCode([DisallowNull] DeviceType obj)
        {
            return obj.Name.Concat(obj.Description).GetHashCode();
        }
    }

    public static DeviceType Copy(DeviceType d)
    {
        return new()
        {
            Name = d.Name,
            Description = d.Description
        };
    }

    public override bool Equals(object obj)
    {
        if (obj is not DeviceType) return base.Equals(obj);

        DeviceType other = (DeviceType)obj;
        return Name == other.Name &&
                Description == other.Description;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return $"""
                    Name = {Name},
                    Description = {Description}
                """;
    }
}
