using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Entities;

#nullable disable

[Owned]
public class Consumable
{
    [Key]
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string URL { get; set; }
    public string DeviceName { get; set; }
    public string Comment { get; set; }

    public class ConsumableComparer : IEqualityComparer<Consumable>
    {
        public bool Equals(Consumable x, Consumable y)
        {
            if (x is null || y is null) throw new NullReferenceException();

            return x.Name == y.Name && 
                x.URL == y.URL &&
                x.DeviceName == y.DeviceName &&
                x.Comment == y.Comment;
                
        }

        public int GetHashCode([DisallowNull] Consumable obj)
        {
            return obj.Name.Concat(obj.DeviceName).GetHashCode();
        }
    }

        public static Consumable Copy(Consumable c)
    {
        return new()
        {
            Name = c.Name,
            URL = c.URL,
            DeviceName = c.DeviceName,
            Comment = c.Comment,
        };
    }

    public override bool Equals(object obj)
    {
        if (obj is not Consumable) return base.Equals(obj);

        Consumable other = (Consumable)obj;
        return Name == other.Name && 
                URL == other.URL &&
                DeviceName == other.DeviceName &&
                Comment == other.Comment;
    }
    
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    
    public override string ToString()
    {
        return $"""
                    Name = {Name},
                    URL = {URL},
                    DeviceName = {DeviceName},
                    Comment = {Comment}
                """;
    }
}
