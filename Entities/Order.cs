using System.ComponentModel.DataAnnotations;

namespace Entities;

#nullable disable
public class Order
{
    [Key]
    public Guid Id { get; set; }

    [Required]
    public Location Location { get; set; }

    [Required]
    public DeviceType DeviceType { get; set; }

    [Required]
    public Consumable Consumable { get; set; }

    public string Comment { get; set; }

    public DateTime DateOfCreation { get; set; }

    public static Order Copy(Order o)
    {
        return new()
        {
            Id = o.Id,
            Location = Location.Copy(o.Location),
            DeviceType = DeviceType.Copy(o.DeviceType),
            Consumable = Consumable.Copy(o.Consumable),
            Comment = o.Comment,
            DateOfCreation = o.DateOfCreation
        };
    }
    public override string ToString()
    {
        return $"""
                    Location = {Location},
                    DeviceType = {DeviceType},
                    Consumable = {Consumable},
                    Comment = {Comment},
                    DateOfCreation = {DateOfCreation}
                """;
    }
}