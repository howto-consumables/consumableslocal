namespace LogicTest;

using Arr = PreArranged;
using Logic;

public class ArchivedOrdersTest
{

    private readonly CoreLogic _objectUnderTest = new();

    private void ClearDatabase()
    {
        using var context = new ConsumablesLocalDbContext();
        context.Database.EnsureDeleted();
        context.Database.EnsureCreated();
        Assert.Empty(context.ArchivedOrders);
    }

    [Fact]
    public void ArchivePendingOrders_on_empty_PendingOrders_has_no_effect_on_PendingOrders()
    {
        var before = _objectUnderTest.GetPendingOrders();

        // act
        _objectUnderTest.ArchivePendingOrders();
        var after = _objectUnderTest.GetPendingOrders();

        // assert
        Assert.Empty(after);
        Assert.Equal(before, after);
    }

    [Fact]
    public void ArchivePendingOrders_Clears_PendingOrders()
    {
        // arrange
        ClearDatabase();
        _objectUnderTest.AddToPendingOrders(Arr.SampleOrder);
        var before = _objectUnderTest.GetPendingOrders();

        // act
        _objectUnderTest.ArchivePendingOrders();
        var after = _objectUnderTest.GetPendingOrders();

        // assert
        Assert.NotEmpty(before);
        Assert.Empty(after);
    }

    [Fact]
    public void GetArchivedOrders_contains_former_PendingOrders()
    {
        // arrange
        ClearDatabase();
        _objectUnderTest.AddToPendingOrders(Arr.SampleOrder);
        var before = _objectUnderTest.GetPendingOrders();
        var archivedBefore = _objectUnderTest
            .GetArchivedOrders().ToList();

        // act
        _objectUnderTest.ArchivePendingOrders();
        var archived = _objectUnderTest.GetArchivedOrders();
        var maybeFound = archived.First(o => o.Id == Arr.SampleOrder.Id);

        // assert
        Assert.NotEmpty(before);
        Assert.NotEmpty(archived);
        Assert.NotNull(maybeFound);
        
    }

        [Fact]
    public void GetArchivedOrders_after_adding_and_archiving_three_similar_items_contains_expected_and_not_identical_items()
    {
        ClearDatabase();
        
        // arrange
         _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);

        var beforeOne = _objectUnderTest.GetPendingOrders().Skip(0).First();
        var beforeTwo = _objectUnderTest.GetPendingOrders().Skip(1).First();
        var beforeThree = _objectUnderTest.GetPendingOrders().Skip(2).First();

        // act
       _objectUnderTest.ArchivePendingOrders();

        // NOTE: test db does not get reset before tests, there may be many other orders
        var one = _objectUnderTest.GetArchivedOrders().First( o => o.Id == beforeOne.Id);
        var two = _objectUnderTest.GetArchivedOrders().First( o => o.Id == beforeTwo.Id);
        var three = _objectUnderTest.GetArchivedOrders().First( o => o.Id == beforeThree.Id);

        // assert
        Assert.NotNull(one);
        Assert.IsType<Guid>(one.Id);

        Assert.NotNull(two);
        Assert.IsType<Guid>(two.Id);

        Assert.NotNull(three);
        Assert.IsType<Guid>(three.Id);

        Assert.NotEqual(one.Id, two.Id);
        Assert.NotEqual(one.Id, three.Id);
        Assert.NotEqual(two.Id, three.Id);

        Assert.Equal(beforeTwo.Id, two.Id);
        Assert.Equal(beforeTwo.Location, two.Location);
        Assert.Equal(beforeTwo.DeviceType, two.DeviceType);
        Assert.Equal(beforeTwo.Consumable, two.Consumable);
        Assert.Equal(beforeTwo.Comment, two.Comment);
        Assert.Equal(beforeTwo.DateOfCreation, two.DateOfCreation);
    }

}