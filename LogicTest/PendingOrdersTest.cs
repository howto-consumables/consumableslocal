namespace LogicTest;

using Arr = PreArranged;
using Logic;
using Entities;

public class PendingOrdersTest
{
    private readonly CoreLogic _objectUnderTest = new();

    [Fact]
    public void AddToPendingOrders_Adds_An_Item()
    {
        // arrange -> done in the constructor

        // act
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);

        var pendingOrders = _objectUnderTest.GetPendingOrders();

        // assert
        Assert.NotEmpty(pendingOrders);
    }

    [Fact]
    public void GetPendingOrders_After_AddToPendingOrders_Contains_Expected_Item()
    {
        // arrange -> done in the constructor

        // act
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);

        var maybeItem = _objectUnderTest.GetPendingOrders()
            .First(o =>
                o.Location == Arr.SampleLocation &&
                o.DeviceType == Arr.SampleDeviceType &&
                o.Consumable == Arr.SampleConsumable &&
                o.Comment == Arr.SampleComment
            );

        // assert
        Assert.NotNull(maybeItem);
        Assert.IsType<Guid>(maybeItem.Id);
    }

    [Fact]
    public void GetPendingOrders_after_adding_three_similar_items_contains_expected_and_not_identical_items()
    {
        // arrange -> done in the constructor

        // act
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);
        _objectUnderTest.AddToPendingOrders(Arr.SampleLocation, Arr.SampleDeviceType,
            Arr.SampleConsumable, Arr.SampleComment);

        var one = _objectUnderTest.GetPendingOrders()
            .First();
        var two = _objectUnderTest.GetPendingOrders()
            .Skip(1)
            .First();
        var three = _objectUnderTest.GetPendingOrders()
            .Skip(2)
            .First();

        // assert
        Assert.NotNull(one);
        Assert.IsType<Guid>(one.Id);

        Assert.NotNull(two);
        Assert.IsType<Guid>(two.Id);

        Assert.NotNull(three);
        Assert.IsType<Guid>(three.Id);

        Assert.NotEqual(one.Id, two.Id);
        Assert.NotEqual(one.Id, three.Id);
        Assert.NotEqual(two.Id, three.Id);
    }


    [Fact]
    public void RemovePendingOrder_removes_order_from_collection()
    {
        // arrange
        var order = new Order
        {
            Location = Arr.SampleLocation,
            DeviceType = Arr.SampleDeviceType,
            Consumable = Arr.SampleConsumable,
            Comment = Arr.SampleComment
        };
        _objectUnderTest.AddToPendingOrders(order);
        var before = _objectUnderTest.GetPendingOrders();

        // act
        _objectUnderTest.RemovePendingOrder(order);
        var after = _objectUnderTest.GetPendingOrders();

        Assert.DoesNotContain(order, after);
    }
}