using Logic;

namespace LogicTest;




public class CoreLogicTest
{
    [Fact]
    public void Object_creation_works()
    {
        var logic = new CoreLogic();
        Assert.IsType<CoreLogic>(logic);
    }
}