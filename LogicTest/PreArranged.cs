
namespace LogicTest;
using Entities;

public static class PreArranged
{
    internal static readonly Location SampleLocation = new()
    {
        Name = "somename location",
        DeviceNames = [ "somedevicename", "someotherdevicename" ],
        Description = "somedescription"
    };

    internal static readonly DeviceType SampleDeviceType = new()
    {
        Name = "somename device",
        Description = "somedescription"
    };

    internal static readonly Consumable SampleConsumable = new()
    {
        Name = "somename consumable",
        URL = "someurl",
        DeviceName = "somedevicename",
        Comment = "somecomment"
    };

    internal static readonly string SampleComment = "somecomment";

    internal static readonly Order SampleOrder = new()
        {
            Id = Guid.NewGuid(),
            Location = SampleLocation,
            DeviceType = SampleDeviceType,
            Consumable = SampleConsumable,
            Comment = SampleComment
        };
}