using Logic;

namespace LogicTest;

public class StringFilterTest
{
    [Fact]
    public void Object_creation_works()
    {
        // Arrange

        #region

        TestClass item1 = new();
        TestClass item2 = new()
        {
            MyString = "somestring or maybe text..."
        };
        TestClass item3 = new()
        {
            MyString = "",
            MyInt = 6
        };
        TestClass item4 = new()
        {
            MyNestedClass = new()
        };
        TestClass item5 = new()
        {
            MyNestedClass = new()
            {
                MyNestedString = "000tHeTexTTT"
            }
        };
        List<TestClass> list = [item1, item2, item3, item4, item5];

        #endregion

        var filterSome = list.SomeFieldContains("some").ToList();
        var filterSix = list.SomeFieldContains("6").ToList();
        var filterNested = list.SomeFieldContains("nested").ToList();
        var filterText = list.SomeFieldContains("text").ToList();

        Assert.Contains(item2, filterSome);
        Assert.Contains(item3, filterSix);
        Assert.Contains(item4, filterNested);
        Assert.Contains(item5, filterNested);
        Assert.Contains(item2, filterText);
        Assert.Contains(item5, filterText);
    }

    class TestClass
    {
        public string? MyString { get; set; }
        public int MyInt { get; set; }

        public NestedClass? MyNestedClass { get; set; }
    }

    class NestedClass
    {
        public string? MyNestedString { get; set; }

        public override string ToString()
        {
            return "NestedClass: " + MyNestedString;
        }
    }
}