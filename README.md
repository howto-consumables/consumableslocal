# ConsumablesLocal

## Summary

This is a simple desktop app, that allows you to find order links for consumables easily.
Start by adding a DeviceType in the mgmt view.

## Usage

In the main view you can select the needed consumables in a guided way. Locations and device types need to be
chosen before you see the according consumables. You can search and leave a comment. Once you have what you want,
click "Order".

<img src="./ImagesForReadme/MainView_initial.png" alt="MainView initial state" width="400" />
<img src="./ImagesForReadme/MainView_device.png" alt="MainView initial state" width="400" />
<img src="./ImagesForReadme/MainView_consumable.png" alt="MainView initial state" width="400" />
<img src="./ImagesForReadme/MainView_final.png" alt="MainView initial state" width="400" />

In "Pending Orders" you see what you ordered with the weblinks available for you to copy and perform the order.
When you are done, you can archive the current pending orders. That way, you will have a history available.

<img src="./ImagesForReadme/PendingView.png" alt="MainView initial state" width="400" />

In "Archived Orders" you have all archived orders since you started using the program. You can filter for text or
dates.

<img src="./ImagesForReadme/ArchivedView.png" alt="MainView initial state" width="400" />

Under "Mgmt" you can add new locations, device types or consumables. In order to create the other two,
the device type must exist first.

<img src="./ImagesForReadme/MgmtView.png" alt="MainView initial state" width="400" />


## Build and publish

Publish

    cd UI
    dotnet publish -c release -f net8.0 -r win-x64 --self-contained true -p:PublishSingleFile=true -p:IncludeNativeLibrariesForSelfExtract=true -o Publish

Change the platform to match your target machine.

## Run

    dotnet build
    cd UI/bin/Debug/net8.0
    dotnet UI.dll

(alternatively use the debug button in an ide when a .axaml file is active)

Avalonia

https://docs.avaloniaui.net/docs/0.10.x/authoring-controls/defining-properties


### To be aware of

The file ./consumables_local.db needs to be in the running directory.

The file ./archived_orders.db needs to be present as well. All four are hard coded in this version.

Create ./consumables_local.db by moving into ./Logic and runnig:

    dotnet ef migrations add InitialCreate
    dotnet ef database update

